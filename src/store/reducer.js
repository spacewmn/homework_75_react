import {POST_DECODE_SUCCESS, POST_ENCODE_SUCCESS} from "./actionTypes";

const initialState = {
    decode: '',
    encode: ''
};

const messageReducer = (state = initialState, action) => {

    switch (action.type) {
        case POST_DECODE_SUCCESS:
            return {...state, decode: action.message.decode};
        case POST_ENCODE_SUCCESS:
            return {...state, encode: action.message.encode}
        default:
            return state;
    }
};

export default messageReducer;