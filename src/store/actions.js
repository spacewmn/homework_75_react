import {POST_DECODE_SUCCESS, POST_ENCODE_SUCCESS} from "./actionTypes";
import axiosApi from "../axiosApi";

export const postDecodeSuccess = (message) => {
    return {type: POST_DECODE_SUCCESS, message}
};

export const postDecode = message => {
    return async dispatch => {
        const response = await axiosApi.post('/decode', message);
        dispatch(postDecodeSuccess(response.data));
    };
};

export const postEncodeSuccess = (message) => {
    return {type: POST_ENCODE_SUCCESS, message}
};

export const postEncode = message => {
    return async dispatch => {
        const response = await axiosApi.post('/encode', message);
        dispatch(postEncodeSuccess(response.data));
    };
};