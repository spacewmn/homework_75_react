import React from 'react';
import CodeMessage from "./containers/CodeMessage";

const App = () => {
    return (
        <div>
          <CodeMessage/>
        </div>
    );
};

export default App;