import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {postDecode, postEncode} from "../store/actions";

const CodeMessage = () => {
    const [state, setState] = useState({
        encode: '',
        decode: '',
        password: ''
    });

    const dispatch = useDispatch();
    const messageDecode = useSelector(state => state.decode);
    const messageEncode = useSelector(state => state.encode);

    const submitFormHandler = e => {
        e.preventDefault();
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    useEffect(() => {
        console.log(messageDecode, "decode");
        setState(prevState => {
            return {...prevState, decode: messageDecode}
        })
    }, [messageDecode]);

    useEffect(() => {
        console.log(messageEncode);
        setState(prevState => {
            return {...prevState, encode: messageEncode}
        })
    }, [messageEncode]);

    const onFormSubmitDecode = () => {
        const obj = {
            message: state.encode,
            password: state.password
        };
        dispatch(postDecode(obj));
    };

    const onFormSubmitEncode = () => {
        const obj = {
            message: state.decode,
            password: state.password
        };
        dispatch(postEncode(obj));
    };

    return (
            <form className="container mt-5" onSubmit={submitFormHandler}>
                <div className="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Decoded message</label>
                    <textarea
                        className="form-control"
                        id="exampleFormControlTextarea1"
                        rows="3"
                        name="encode"
                        onChange={inputChangeHandler}
                        value={state.encode}
                    />
                </div>
                <div className="form-group mx-sm-3 mb-2">
                    <label htmlFor="inputPassword2" className="sr-only">Password</label>
                    <input
                        type="password"
                        className="form-control"
                        id="inputPassword2"
                        name="password"
                        onChange={inputChangeHandler}
                    />
                </div>
                <button type="submit" className="btn btn-primary mb-2 mr-2"  onClick={onFormSubmitDecode}>&darr;</button>
                <button type="submit" className="btn btn-primary mb-2" onClick={onFormSubmitEncode}>&uarr;</button>
                <div className="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Encoded message</label>
                    <textarea
                        className="form-control"
                        id="exampleFormControlTextarea1"
                        name="decode"
                        rows="3"
                        onChange={inputChangeHandler}
                        value={state.decode}
                    />
                </div>
            </form>
    );
};

export default CodeMessage;